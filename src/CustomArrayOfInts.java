import java.util.Arrays;

public class CustomArrayOfInts {
    private int[] data = new int[1];  // only grows by doubling size, never shrinks
    private int size = 0; // how many items do you really have

    public int Size() {return size;}
    public void add(int value) {
        if(size == data.length){
            int[] newData = new int[size*2];
            for(int i = 0; i<size; i++){
                newData[i] = data[i];
            }
            data = newData;
        }
        data[size] = value;
        size ++;
    }
    public void deleteByIndex(int index){
        if(index >= size || index < 0){
            System.out.println("Index is out of the boundary of array");
            return;
        }
        else {
            for(int i = index; i<size-1;i++)
            {
                if(index == size - 1) {
                    data[index] = 0;
                    break;
                }
                data[i] = data[i+1];
            }
        }
        size --;
    }
    public boolean deleteByValue(int value){ // delete first value matching, true if value found, false otherwise
        boolean isFound = false;
        int index = -1;
        for(int i = 0; i < size; i++){
            if(data[i] == value){
                index = i;
                isFound = true;
                break;
            }
        }
        if(isFound){
            deleteByIndex(index);
        }
        return isFound;
    }
    public void insertValueAtIndex(int value, int index){
        if(index >= size || index < 0){
            System.out.println("Index is out of the boundary of array");
        }
        else {
            if(size == data.length){
                data = new int[size*2];
            }
            for(int i = size-1; i >= index;i--)
            {
                data[i+1] = data[i];
            }
            data[index] = value;
            size ++;
        }
    }
    public void clear(){
        size = 0;
    }
    public int get(int index){  // may throw IndexOutOfBoundsException
        if(index < size && index >= 0)
            return data[index];
        else {
            throw new IndexOutOfBoundsException();
        }
    }
    public int[] getSlice(int startIdx, int length){  // may throw IndexOutOfBoundsException
        if(startIdx < size && startIdx >= 0 && (startIdx + length) <= size){
            int[] slice = new int[length];
            for(int i=0; i<length; i++){
                slice[i] = data[startIdx+i];
            }
            return slice;
        }
        else {
            throw new IndexOutOfBoundsException();
        }
    }
    @Override
    public String toString(){ // returns String similar to: [3,5,6,-23]
        String line = "[";
        for(int i = 0; i < size; i++){
            line += i==0? data[i] : "," + data[i];
        }
        line += "]";
        return line;
    }
}
