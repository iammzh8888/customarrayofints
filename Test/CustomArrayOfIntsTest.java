import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomArrayOfIntsTest {

    @Test
    public void Size() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        assertEquals(0,ca.Size());
        ca.add(0);
        assertEquals(1,ca.Size());
        ca.add(1);
        assertEquals(2,ca.Size());
        ca.add(2);
        assertEquals(3,ca.Size());
        ca.add(3);
        assertEquals(4,ca.Size());
        ca.add(4);
        assertEquals(5,ca.Size());
    }

    @Test
    public void add() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(3);
        assertEquals(1,ca.Size());
        assertEquals(3,ca.get(0));
        ca.add(2);
        assertEquals(2,ca.Size());
        assertEquals(3,ca.get(0));
        assertEquals(2,ca.get(1));
        ca.add(1);
        assertEquals(3,ca.Size());
        assertEquals(3,ca.get(0));
        assertEquals(2,ca.get(1));
        assertEquals(1,ca.get(2));
        ca.add(4);
        assertEquals(4,ca.Size());
        assertEquals(3,ca.get(0));
        assertEquals(2,ca.get(1));
        assertEquals(1,ca.get(2));
        assertEquals(4,ca.get(3));
        ca.add(5);
        assertEquals(5,ca.Size());
        assertEquals(3,ca.get(0));
        assertEquals(2,ca.get(1));
        assertEquals(1,ca.get(2));
        assertEquals(4,ca.get(3));
        assertEquals(5,ca.get(4));
    }

    @Test
    public void deleteByIndex() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(0);
        ca.add(1);
        ca.add(2);
        ca.add(3);
        ca.add(4);
        assertEquals(5,ca.Size());
        ca.deleteByIndex(2);
        assertEquals(4,ca.Size());
        assertEquals(0,ca.get(0));
        assertEquals(1,ca.get(1));
        assertEquals(3,ca.get(2));
        assertEquals(4,ca.get(3));
    }

    @Test
    public void deleteByValue() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(1);
        ca.add(2);
        ca.add(3);
        ca.add(4);
        ca.add(2);
        assertEquals(5,ca.Size());
        ca.deleteByValue(2);
        assertEquals(4,ca.Size());
        assertEquals(1,ca.get(0));
        assertEquals(3,ca.get(1));
        assertEquals(4,ca.get(2));
        assertEquals(2,ca.get(3));
    }

    @Test
    public void insertValueAtIndex() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(1);
        ca.add(2);
        ca.add(3);
        ca.add(4);
        ca.add(5);
        assertEquals(5,ca.Size());
        ca.insertValueAtIndex(6,2);
        assertEquals(6,ca.Size());
        assertEquals(1,ca.get(0));
        assertEquals(2,ca.get(1));
        assertEquals(6,ca.get(2));
        assertEquals(3,ca.get(3));
        assertEquals(4,ca.get(4));
        assertEquals(5,ca.get(5));
    }

    @Test
    public void clear() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(1);
        ca.add(2);
        ca.add(3);
        ca.add(4);
        ca.add(5);
        assertEquals(5,ca.Size());
        ca.clear();
        assertEquals(0,ca.Size());
    }

    @Test
    public void get() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(3);
        ca.add(2);
        ca.add(1);
        ca.add(4);
        ca.add(5);
        assertEquals(3,ca.get(0));
        assertEquals(2,ca.get(1));
        assertEquals(1,ca.get(2));
        assertEquals(4,ca.get(3));
        assertEquals(5,ca.get(4));
    }

    @Test
    public void getSlice() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(3);
        ca.add(2);
        ca.add(1);
        ca.add(4);
        ca.add(5);
        int[] slice = ca.getSlice(1,3);
        assertEquals(3,slice.length);
        assertEquals(2,slice[0]);
        assertEquals(1,slice[1]);
        assertEquals(4,slice[2]);
    }

    @Test
    public void testToString() throws Exception{
        CustomArrayOfInts ca = new CustomArrayOfInts();
        ca.add(0);
        ca.add(1);
        ca.add(2);
        ca.add(3);
        ca.add(-4);
        assertEquals("[0,1,2,3,-4]",ca.toString());
    }
}